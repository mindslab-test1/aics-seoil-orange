## Seoil - Orange



### Purpose

제품 품질 관리 PDF를 한 번에 묶음으로 업로드 할 때 각 PDF의 Table 및 모델 명 등을 가진 rule-based model에 입각하여 xlsx 및 웹 페이지로 정리, 출력해주는 웹 어플리케이션입니다.



### Command

requirements.txt 내의 모듈을 pip로 설치 이후 web 폴더 내에서

```shell
python3 ORANGE_web_server.py
```



### Project Structure

1. ##### web

   - web 템플릿 및 js,css 등의 static resources 제공 코드
   - ORANGE_web_server.py
     - 모든 PDF변환, rule 변경 등의 요청
     - js나 css 등의 resources 반환
     - DB 상에 로그인 로그 및 PDF변환 로그 insert
     - 로그인에 대해 ID 및 PW validation
   - dir:config
     - db.py (db정보 및 설정)
   - dir:static
     - css, js, font, images 등 static resources
   - dir:templates
     - login.html : 처음 로그인 화면
     - return.html : 로그인 이후 PDF변경 화면

2. ##### test_files

   - 웹에서 PDF변환 테스트를 위한 예시 파일
   - 실측을 통한 자료이기 때문에 해당 프로젝트 외에 외부에 노출하면 안 되는 자료입니다.

3. ##### pdf_modules

   - pdf 변환에 사용되는 여러 스크립트
   - dir:rules
     - file_cat_rules.csv : 파일 명에 포함된 텍스트로 현재 파일이 어떤 카테고리의 파일인지 확인
     - information_to_keyval.csv : KEY값 중에 예외적으로 저장해야 하는 값에 대한 정의
     - model_rules.csv : 파일 이름에서 규칙을 통해 모델을 특정하는 규칙
     - overall_model_rules_original.csv : rulemaking 폴더 내의 after_...의 파일의 원본. 실제로 변환에 사용되지 않으며 후에 after_...파일이 깨지거나 다시 원복이 필요할 때에 해당 csv를 rulemaking 폴더 내에 파일 이름을 변경하고 넣으면 다시 반영
     - xlsx_front_keys.csv : 웹 상에서 무조건 (값이 없더라도) 맨 앞에 등장해야 하는 KEY값들의 목록
     - dir : rulemaking
       - after_overall_model_rules_tmp.csv : 
       - rulemaking.py : overall_model_rules 생성때 사용했던 일회성 스크립트 (deprecated)



### Pipeline (file upload)

1. ##### ORANGE_web_server.py : upload()

   - web/tmp 내부에 uuid의 이름을 가진 디렉토리 생성
   - 해당 디렉토리에 방금 업로드 된 모든 파일들 저장
   - returnstr = overall_pick_check() 호출

2. ##### overall_pdf_process.py : overall_pick_check()

   - 전체 모델 룰 로드 (f_overall_rules)
   - README #1 : 현재 파일들의 연관성을 파악하여 현재 들어온 모델이 몇 종류인지, 엑셀에 들어가야 할 KEY가 몇 개인지 파악
   - README #2 : 전체 파일 중 연관된 파일끼리 묶어서 변환 프로세스 진행
   - 각 파일에 대해 file_to_str() 호출로 KEY,Value를 변환하여 출력





### Input Files (relation)

XLSX 상의 한 줄은 여러개의 PDF가 합쳐져 하나의 라인을 만들어 냅니다. 예시로 test_files/TEMAH/0122 내부의 파일을 살펴보면 아래와 같습니다.

```
1-1. A11119.PDF
1-2. A11119.xlsx
1-3. A11519.PDF
1-4. A11519.xlsx
1-5. A21119.PDF
1-6. A21119.xlsx
1-7. A21519.PDF
1-8. A21519.xlsx
1-9. TEMAH-Lot.A11119(뾢딇뙚뜽_ E441_HFK4L-783).PDF
1-10. TEMAH-Lot.A11119-A21119갂A11519-A21519(Para).pdf
1-11. TEMAH-Lot.A11119-A21119갂A11519-A21519(뙱뿿COA).pdf
1-12. TEMAH-Lot.A11519(뾢딇뙚뜽_ E490_HFK4L-862).PDF
1-13. TEMAH-Lot.A21119(뾢딇뙚뜽_ E468_HFK4L-820).PDF
1-14. TEMAH-Lot.A21519(뾢딇뙚뜽_ E611_HFK4L-1083).PDF
```

여기서 xlsx파일은 업로드 대상에서 제외되며 나머지 pdf 파일에 대해서 파일 분류를 내려보면 아래와 같습니다. (file category는 Original, Analytical, COA_Analytical, Process, Canister 가 있으며, 이는 파일 내에 포함된 문자열로 검색합니다. rule는 `pdf_modules/rules/file_cat_rules.csv` 에 있습니다.)

```
2-1. A11119.PDF (Original)
2-2. A11519.PDF (Original)
2-3. A21119.PDF (Original)
2-4. A21519.PDF (Original)
2-5. TEMAH-Lot.A11119(뾢딇뙚뜽_ E441_HFK4L-783).PDF (Canister)
2-6. TEMAH-Lot.A11119-A21119갂A11519-A21519(Para).pdf  (Process)
2-7. TEMAH-Lot.A11119-A21119갂A11519-A21519(뙱뿿COA).pdf (COA_Analytical)
2-8. TEMAH-Lot.A11519(뾢딇뙚뜽_ E490_HFK4L-862).PDF (Canister)
2-9. TEMAH-Lot.A21119(뾢딇뙚뜽_ E468_HFK4L-820).PDF (Canister)
2-10. TEMAH-Lot.A21519(뾢딇뙚뜽_ E611_HFK4L-1083).PDF (Canister)
```

여기서 6,7 filename에 있는 - 표시는 범위 표시입니다. file_cat_rules.csv에 의해 파일 이름으로 각 파일의 카테고리를 확인하였으니 그 다음엔 파일 연관성 대로 묶습니다.

```
3-1. A11119.PDF (Original)
   TEMAH-Lot.A11119(뾢딇뙚뜽_ E441_HFK4L-783).PDF (Canister)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(Para).pdf  (Process)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(뙱뿿COA).pdf (COA_Analytical)
   
3-2. A11519.PDF (Original)
   TEMAH-Lot.A11519(뾢딇뙚뜽_ E490_HFK4L-862).PDF (Canister)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(Para).pdf  (Process)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(뙱뿿COA).pdf (COA_Analytical)

3-3. A21119.PDF (Original)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(Para).pdf  (Process)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(뙱뿿COA).pdf (COA_Analytical)
   TEMAH-Lot.A21119(뾢딇뙚뜽_ E468_HFK4L-820).PDF (Canister)
   
3-4. A21519.PDF (Original)
   TEMAH-Lot.A21519(뾢딇뙚뜽_ E611_HFK4L-1083).PDF (Canister)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(Para).pdf  (Process)
   TEMAH-Lot.A11119-A21119갂A11519-A21519(뙱뿿COA).pdf (COA_Analytical)
```

총 4개의 목록으로 들어갔으며, 이는 결과적으로 xlsx 상으로 4줄의 결과물로 도출됩니다.

2-6과 2-7의 경우 범위설정으로 인해 서로 다른 3-1과 3-2, 3-3, 3-4에 모두 값을 공유합니다. Pipeline의 README #2 에서의 작업에서 relative_files의 경우 for문을 4번 돌며 3-1, 3-2, 3-3, 3-4의 값을 한 번씩 가집니다.



### Input Files (filename)

위의 A12349.pdf 라는 이름에 대해서 아래와 같은 정보를 rule로 인해 파악합니다.

- ##### A (TEMAH 모델)

  - model_rules.csv에 저장되어있으며, 해당 파일 이름의 첫 글자를 통해 해당 파일의 모델을 파악합니다.
  - 파일 이름의 처음을 보는 것이 아니라, 파일 이름의 regex를 찾고, 해당 regex의 처음을 보는 것이기 때문에 꼭 파일 이름의 맨 처음이 모델 번호일 필요 없습니다. (예를들어 aaa_A11119.PDF 라는 이름도 상관 없습니다.)

- ##### 1 (번호)

  - 파악에 큰 의미는 없습니다.

- ##### 23 (계측 날짜)

  - 계측 날짜의 일 수를 나타냅니다.

- ##### 4 (계측 월)

  - 계측 날짜의 월을 나타냅니다.
  - 10월의 경우 X, 11월은 Y, 12월은 Z로 저장됩니다.

- ##### 9 (계측 연도)

  - 계측 날짜의 연도를 나타냅니다.

이로 인해 해당 PDF의 날짜는 19년 4월 23일 것을 알 수 있으며, 이는 XLSX를 출력할 때에 MANU_DATE와 TEST_DATE라는 값으로 고정되어 나타납니다.





### ERROR Handling

1. 표에서 입력한 값이 나오지 않는다

   - 해당 프로세스는 regex로 테이블 내의 Key값과 Value값을 도출합니다.

   - PDF를 작성할 때에 정말 작은 개행문자가 존재하거나, Key와 Value가 같은 행에 있지 않을 경우도 있습니다.

   - file_to_pdf.py 에서 해당 PDF파일을 function test block에서 테스트 해보고 제대로 Key와 Value를 반환하는지 확인합니다.

   - 테이블의 값이 누락되었을 경우 두 가지의 print를 확인해볼 수 있습니다. (file_to_pdf.py Line 125 : README #3)

     1. pdf_page (PDF -> String) 직후의 값

     2. tmp_key_val (Key를 모델에 따라 변화시키기 전, 테이블 자체에서 도출한 KEY와 Value)

        

2. 파일을 업로드 했을 때에 특정 파일의 값이 xlsx 상으로 보여지지 않는다

   - 연관된 파일의 룰을 찾지 못 했을 경우 발생할 수 있습니다.
   - README #2 (overall_pdf_process.py Line 58) 에서 overall_files 를 출력하여 제대로 묶였는지 파악해볼 수 있습니다.
   - 파일 이름이 연관성을 만들기에 충분하지 않을 수 있습니다.
   - 카테고리를 파악하지 못 해 Original이 아닌 파일이 Original로 분류되어 같은 Original끼리의 Key로 덮어씌워진 것일 수 있습니다.

