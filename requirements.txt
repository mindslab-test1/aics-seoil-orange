APScheduler==3.7.0
click==7.1.2
Flask==1.1.2
Flask-Cors==3.0.10
grpcio==1.37.0
grpcio-tools==1.37.0
itsdangerous==1.1.0
Jinja2==2.11.3
MarkupSafe==1.1.1
numpy==1.19.5
opencv-python==4.5.1.48
pdftotext==2.1.5
protobuf==3.15.8
PyMySQL==1.0.2
pytz==2021.1
six==1.15.0
tzlocal==2.1
Werkzeug==1.0.1
