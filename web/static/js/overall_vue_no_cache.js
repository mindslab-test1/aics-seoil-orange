// var forurl = "http://127.0.0.1:33353"
var vm = new Vue({
            el: '#onscreen',
            data: {
                status:0,
                // 0 -> default (before upload)
                // 1 -> upload end
                // 2 -> after upload ( waiting response )
                // 3 -> change positioning
                return_text:'',
                table_list:[],
                tempOverall:[],
                value_error_list:[],
                filenames:[],

                //change position status
                nowchangeindex:0,

                //Excel
                nowExcelName:"",



                //now_screen
                //0 -> CSV & Excel
                //1 -> Rule
                nowScreen:0,
                firstlyRulePage:0,

                nowRULECat:'0',//0->overall       1->ModelRule        2->CommonKey
                modelList:[],
                nowSelected:"",


                returnRuleCSV:"",
                overallRuleList:[],
                modelRuleList:[],
                commonKeyList:[],

                nowOverallSeeCatList:[],


                nowChangeDom:0,

            }
        })
function uploadBtnClick() {
    document.getElementById("ex_file").click();
}
function uploadBtnClick2(){
    document.getElementById("fileuploader").click();
}
function AfterUpload(){
    document.getElementById("submitbut").click();
}

function uploadFile(){
    if(vm.status==2){
        alert("서버에서 변환작업 중입니다.")
        return 0;
    }
    let fileInput = document.getElementById("ex_file");
    let nowfilelength = fileInput.files.length
    if(nowfilelength<1){
        alert("No File");
        return 0;
    }
    let url ="../upload";
    vm.status = 2
    let formData = new FormData();
            // formData.append("file" , fileInput.files[0]);
    for(let TT = 0; TT<nowfilelength; TT++){
        formData.append("file[" + TT.toString() +  "]", fileInput.files[TT])
    }
        // formData.append("file" , fileInput.files);
        let xhr = new XMLHttpRequest();
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            vm.status = 1
            vm.return_text = ""
            vm.return_text = (xhr.responseText)
            vm.table_list=[]
            vm.tempOverall=[]
            vm.filenames=[]
            vm.value_error_list=[]

            let first_text = vm.return_text.split("<CELL><LINE>")
            for(let T in first_text){
                vm.table_list.push(first_text[T].split("<CELL>").splice(4))
                if(first_text[T].split("<CELL>")[0].length<20){vm.filenames.push(first_text[T].split("<CELL>")[0])}
                else{vm.filenames.push("..." + first_text[T].split("<CELL>")[0].substr(-17))}
            }
            for(let K in vm.table_list[0]){
                vm.tempOverall.push("")
            }
            // alert(vm.table_list)

        //    value_error_check
            for(let X = 0; X<vm.table_list.length; X++){
                let tmp_value_error_list = []
                for(let Y = 0; Y<vm.table_list[X].length; Y++){
                    if(vm.table_list[X][Y].includes('SPEC ERROR**')){
                        vm.table_list[X][Y] = vm.table_list[X][Y].split("**")[1]
                        tmp_value_error_list.push(true)
                    }
                    else{
                        tmp_value_error_list.push(false)
                    }
                }
                vm.value_error_list.push(tmp_value_error_list)
            }
        }
        xhr.send(formData);
}

function resetPage(){
    vm.nowUploadStatus=0;
    vm.nowSample=1
    vm.nowImgName=""
    vm.nowloading=0
}

function overallChange(indexToChange){
    let toggletmp = false
    let tmpvalue = vm.table_list[1][indexToChange]
    for(let T = 2; T<vm.table_list.length; T++){
        if(tmpvalue!=vm.table_list[T][indexToChange]) toggletmp = true
    }
    if(toggletmp){
        let result = confirm("해당 행에 서로 다른 값이 있습니다. 진행 하시겠습니까?");
        if(result) toggletmp = false
    }
    if(!toggletmp){
        for(let T = 1; T<vm.table_list.length; T++){
            vm.table_list[T].splice(indexToChange,1,vm.tempOverall[indexToChange])
            vm.value_error_list[T].splice(indexToChange,1,false)

        }
    }
    vm.tempOverall[indexToChange] = ""
}

function change_position_on(fromindex){
    if(vm.status==1){
        vm.nowchangeindex = fromindex;
        vm.status=3;
    }
}

function change_to(toindex){
    vm.status=1
    let result = ""
    if(toindex==-1) {result = confirm(vm.table_list[0][vm.nowchangeindex] +" 열의 값을 " + vm.table_list[0][0] + " 열 왼쪽으로 옮깁니다.");}
    else{result = confirm(vm.table_list[0][vm.nowchangeindex] +" 열의 값을 " + vm.table_list[0][toindex] + " 열 오른쪽으로 옮깁니다.");}
    if(result){
        if(toindex<vm.nowchangeindex){
             for(let tmpT = 0; tmpT<vm.table_list.length; tmpT++){
                 let tmpElem = vm.table_list[tmpT].splice(vm.nowchangeindex,1)
                 vm.table_list[tmpT].splice(toindex+1,0,tmpElem)
            }
        }
        else{
             for(let tmpT = 0; tmpT<vm.table_list.length; tmpT++){
                 let tmpElem = vm.table_list[tmpT].splice(vm.nowchangeindex,1)
                 vm.table_list[tmpT].splice(toindex,0,tmpElem)
            }
        }
    }
    if(result){
        if(toindex<vm.nowchangeindex){
             for(let tmpT = 0; tmpT<vm.value_error_list.length; tmpT++){
                 let tmpElem =(vm.value_error_list[tmpT][vm.nowchangeindex])
                 vm.value_error_list[tmpT].splice(vm.nowchangeindex,1)
                 vm.value_error_list[tmpT].splice(toindex+1,0,tmpElem)
            }
        }
        else{
             for(let tmpT = 0; tmpT<vm.value_error_list.length; tmpT++){
                 let tmpElem = (vm.value_error_list[tmpT][vm.nowchangeindex])
                 vm.value_error_list[tmpT].splice(vm.nowchangeindex,1)
                 vm.value_error_list[tmpT].splice(toindex,0,tmpElem)
            }
        }
    }
    vm.nowchangeindex = 0;
}

function deleteLine(index){
    let result=confirm(vm.table_list[0][index] +" 열의 값을 지우겠습니까?")
    if(result){
         for(let tmpT = 0; tmpT<vm.table_list.length; tmpT++){
            vm.table_list[tmpT].splice(index,1)
        }
    }
}

function newLine(index){
    for(let tmpT = 0; tmpT<vm.table_list.length; tmpT++){
        vm.table_list[tmpT].splice(index,0,"")
        vm.value_error_list[tmpT].splice(index,0,false)
    }
}


function saveXlsx(){
    var wb = XLSX.utils.book_new();
    var newWorksheet = excelHandler.getWorksheet();
    XLSX.utils.book_append_sheet(wb, newWorksheet, excelHandler.getSheetName());
    var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), excelHandler.getExcelFileName());
}
var excelHandler = {
		getExcelFileName : function(){
		    return vm.nowExcelName + ".xlsx";
		},
		getSheetName : function(){
			return 'autocreated_sheet';
		},
		getExcelData : function(){
			return vm.table_list;
		},
		getWorksheet : function(){
			return XLSX.utils.aoa_to_sheet(this.getExcelData());
		}
}
function s2ab(s) {
    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    var view = new Uint8Array(buf);  //create uint8array as viewer
    for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
    return buf;
}


function onchangeXLSXName(){
    vm.nowExcelName = vm.nowExcelName.replace(/[^A-Za-z0-9_-]/g,"")
}

function toRuleCheck(){
    if(vm.status==2){
        alert("서버에서 변환작업 중입니다.")
        return 0;
    }

    if(vm.firstlyRulePage==0){
        vm.nowScreen = 1
        let url ="../rules";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            vm.returnRuleCSV = (xhr.responseText)
            let overallCsv = vm.returnRuleCSV.split("<FILE>")
            let overallRules = overallCsv[1].split("<LINE>")
            let modelRules = overallCsv[2].split("<LINE>")
            let commonKeys = overallCsv[3].split("<LINE>")

            vm.nowRULECat='0'
            vm.modelList=[]
            vm.overallRuleList=[]
            vm.modelRuleList=[]
            vm.commonKeyList=[]
            vm.nowSelected="ACP-2"
            vm.nowOverallSeeCatList=[]

            for(let i = 1; i < overallRules.length; i++){
                if(i==1){
                    vm.overallRuleList.push(["제품명","PDF 항목","Excel 항목","최소값","최대값", "고정값"])
                }else{
                    let tmpPushElem = overallRules[i].split("<SPLIT>")
                    vm.overallRuleList.push(tmpPushElem)
                    if(vm.modelList.indexOf(tmpPushElem[0])==-1){
                        vm.modelList.push(tmpPushElem[0])
                    }
                }
            }
            for(let i = 0; i < modelRules.length; i++){
                if(i==0){
                    vm.modelRuleList.push(["제품명","첫 글자"])
                }
                else
                {
                    vm.modelRuleList.push(modelRules[i].split("<SPLIT>"))
                }
            }
            for(let i = 0; i < commonKeys.length; i++){
                if(i==0)vm.commonKeyList.push(["고정 KEY"])
                if(i<=4){
                    continue
                }
                vm.commonKeyList.push([commonKeys[i]])
            }
            nowSeeCatListChange("ACP-2")

        }
        xhr.send(formData);
    }else{
        vm.nowScreen = 1
        vm.nowSelected="ACP-2"
        nowSeeCatListChange("ACP-2")
    }
}

function nowSeeCatListChange(nowCat){
    vm.nowOverallSeeCatList=[]
    vm.nowOverallSeeCatList.push(vm.overallRuleList[0])
    for (let i = 1; i<vm.overallRuleList.length; i++){
        if(vm.nowSelected == vm.overallRuleList[i][0]){
            vm.nowOverallSeeCatList.push(vm.overallRuleList[i])
        }
    }
}

function off() {
  document.getElementById("overlay").style.display = "none";
}


function overallrule_move(upBool, nowIndex){
    if (upBool) {       //위로
        if(nowIndex>1){
            let tmpSplice = vm.nowOverallSeeCatList.splice(nowIndex,1)
            vm.nowOverallSeeCatList.splice(nowIndex-1,0,tmpSplice[0])
        }
    }else{              //아래로
        if(nowIndex!=vm.nowOverallSeeCatList.length){
            let tmpSplice = vm.nowOverallSeeCatList.splice(nowIndex,1)
            vm.nowOverallSeeCatList.splice(nowIndex+1,0,tmpSplice[0])
        }
    }
}

function checkValueValidation(stringText){
    let n = /^([+-]?((\.\d+)|(\d+(\.\d+)?)))$/;
    let m = /^\s*-\s*$/;
    return n.test(stringText) || m.test(stringText)
}

function orderSave(){
    for(let ii = 1; ii<vm.nowOverallSeeCatList.length; ii++){
        if(!(checkValueValidation(vm.nowOverallSeeCatList[ii][3].toString()))) {alert("최소값에 잘못된 값이 있습니다."); return 0}
        if(!(checkValueValidation(vm.nowOverallSeeCatList[ii][4].toString()))) {alert("최대값에 잘못된 값이 있습니다."); return 0}
        if(!(checkValueValidation(vm.nowOverallSeeCatList[ii][5].toString()))) {alert("고정값에 잘못된 값이 있습니다."); return 0}
    }

    let result=confirm("저장 이후에 변환하는 PDF 파일은 현재 하단의 순서에 맞게 정렬됩니다. 계속 하시겠습니까?")
    if(result){
        let postString = vm.nowOverallSeeCatList[1][0]
        for(let i = 1 ; i<vm.nowOverallSeeCatList.length; i++){
            postString += "<SPLIT>" + vm.nowOverallSeeCatList[i][1] + "<SPLIT_VAL>" + vm.nowOverallSeeCatList[i][3]
                                                                    + "<SPLIT_VAL>" + vm.nowOverallSeeCatList[i][4]
                                                                    + "<SPLIT_VAL>" + vm.nowOverallSeeCatList[i][5]
        }
        let url ="../ordersave";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("orderStr" , postString);
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="GOOD"){
                alert("성공적으로 저장되었습니다.")
                toRuleCheck()
            }
        }
        xhr.send(formData);
    }
}



function returnXlsx(){
    vm.nowScreen = 0
    vm.status = 0
    vm.return_text = ''
    vm.table_list = []
    vm.tempOverall = []
    vm.value_error_list = []
    vm.filenames = []
    vm.nowchangeindex = 0
    vm.nowExcelName = ""
    vm.nowScreen = 0
    vm.firstlyRulePage = 0
    vm.nowRULECat = '0'
    vm.modelList = []
    vm.nowSelected = ""
    vm.returnRuleCSV = ""
    vm.overallRuleList = []
    vm.modelRuleList = []
    vm.commonKeyList = []
    vm.nowOverallSeeCatList = []
}