import errno
import os
import datetime
import uuid
import atexit
import sys


sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from web.config.db import make_db_collection
from flask import Flask, render_template, request, send_file
from flask_cors import CORS
from apscheduler.schedulers.background import BackgroundScheduler
from pdf_modules.overall_pdf_process import *
import shutil
import csv
from operator import itemgetter


class MyFlask(Flask):
    """
    Jinja Option을 반영하여 Flask를 정의
    cdnjs로 웹페이지 내에서 Vue를 사용하기 때문에 해당 Vue 문법과 서로 맞도록 함
    """
    jinja_options = Flask.jinja_options.copy()
    jinja_options.update(dict(
        block_start_string='{%',
        block_end_string='%}',
        variable_start_string='((',
        variable_end_string='))',
        comment_start_string='{#',
        comment_end_string='#}',
    ))


"""
어플리케이션 시작
static_folder 및 Upload, Make 폴더는 모두 해당 파이썬 스크립트 위치에 대한 상대경로
"""
app = MyFlask(__name__, static_folder="./static")
CORS(app)
app.config['UPLOAD_FOLDER'] = "tmp/uploaded"  # WEB->UPLOAD_FOLDER
app.config['MAKE_FOLDER'] = "tmp/make"


def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))


def get_file(filename):  # pragma: no cover
    try:
        src = os.path.join(root_dir(), filename)
        return open(src).read()
    except IOError as exc:
        return str(exc)


@app.route('/')
def index():
    return render_template('login.html')


@app.route('/login', methods=['GET'])
def login_index():
    return render_template('login.html')


@app.route('/login', methods=['POST'])
def login():
    """
    로그인 시도 시에 보내는 매핑
    로그인 성공 시에 LOGS에 로그인 이력 남김
    :return:
    로그인 실패 시 : login.html  (처음 화면)
    로그인 성공 시 : return.html (PDF 변환 화면)
    """
    try:
        str_id = request.form['id']
        str_pw = request.form['pw']

        db = make_db_collection()
        cursor = db.cursor()
        sql = """
                   SELECT *
                   FROM USER
                   WHERE USER_ID = '%s' AND USER_KEY = '%s'
               """ % (str(str_id), str(str_pw))
        cursor.execute(sql)
        result = cursor.fetchall()

        if result[0][1] == 0:
            now = datetime.datetime.now()
            nowDatetime = str(now.strftime('%Y%2m%2d%2H%2M%2S'))
            sql = """
                               INSERT INTO LOGS(LOGIN_ID,year,month,day,hour,minute,second,type)
                               VALUES('%s',%s,%s,%s,%s,%s,%s,1)
                           """ % (
            str(str_id), nowDatetime[:4], nowDatetime[4:6], nowDatetime[6:8], nowDatetime[8:10], nowDatetime[10:12],
            nowDatetime[12:14])
            cursor.execute(sql)
            db.commit()
            db.close()
            return render_template('return.html')

        return render_template('login.html')
    except:
        now = datetime.datetime.now()
        nowDatetime = str(now.strftime('%Y%2m%2d%2H%2M%2S'))
        sql = """
                                       INSERT INTO LOGS(LOGIN_ID,year,month,day,hour,minute,second,type)
                                       VALUES('%s',%s,%s,%s,%s,%s,%s,0)
                                   """ % (
            str(str_id), nowDatetime[:4], nowDatetime[4:6], nowDatetime[6:8], nowDatetime[8:10], nowDatetime[10:12],
            nowDatetime[12:14])
        cursor.execute(sql)
        db.commit()
        db.close()
        return render_template('login.html')


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    """
    업로드 화면에서 PDF를 업로드
    PDF 변환 수를 LOGS 테이블에 남김
    :return:
    화면상에서 보여줄 표의 정보를 가진 텍스트
    """
    filelist = list()
    for P in request.files.to_dict().keys():
        filelist.append(request.files[P])
    directory_name = str(uuid.uuid4())
    try:
        os.makedirs(os.path.join(app.config['UPLOAD_FOLDER'], directory_name))
    except OSError as e:
        if e.errno != errno.EEXIST:
            print("Failed to create directory")
            raise

    for fn in filelist:
        filename = (fn.filename)
        fn.save(os.path.join(app.config['UPLOAD_FOLDER'], directory_name, filename))

    returnstr = overall_pick_check(os.path.join(app.config['UPLOAD_FOLDER'], directory_name))
    # returnstr : 유저 화면 상에 보여줄 텍스트를 string 2차원 리스트로 가진 값
    tmp_retstr_top = returnstr[0]
    del returnstr[0]
    for T in returnstr:
        T[2] = T[15] + T[18]
    returnstr = sorted(returnstr, key=itemgetter(2), reverse=False)
    returnstr.insert(0, tmp_retstr_top)
    overall_return = ""
    for T in returnstr:
        overall_return += "<LINE>"
        for M in T:
            overall_return += str(M) + "<CELL>"
    overall_return = overall_return[6:-6]

    shutil.rmtree(os.path.join(app.config['UPLOAD_FOLDER'], directory_name))

    db = make_db_collection()
    cursor = db.cursor()
    now = datetime.datetime.now()
    nowDatetime = str(now.strftime('%Y%2m%2d%2H%2M%2S'))
    sql = """
                                  INSERT INTO LOGS(LOGIN_ID,year,month,day,hour,minute,second,type,PDFS)
                                  VALUES('seoil',%s,%s,%s,%s,%s,%s,2,%s)
                              """ % (
    nowDatetime[:4], nowDatetime[4:6], nowDatetime[6:8], nowDatetime[8:10], nowDatetime[10:12], nowDatetime[12:14],
    str(len(filelist)))
    cursor.execute(sql)
    db.commit()
    db.close()

    return overall_return


@app.route('/rules', methods=['POST'])
def getrules():
    """
    현재 적용되고있는 룰 확인 페이지
    홈페이지 내의 우측 상단 rule check애서 호출
    :return:
    rule 관련 텍스트
    js에서 텍스트 파싱해서 화면으로 출력
    """
    returntext = ""

    f = open('../pdf_modules/rules/rulemaking/after_overall_model_rules_tmp.csv')
    rdr = csv.reader(f)
    returntext += "<FILE>"
    for line in rdr:
        returntext += "<LINE>" + line[0] + "<SPLIT>" + line[1] + "<SPLIT>" + line[2] + "<SPLIT>" + line[5] + "<SPLIT>" + \
                      line[6] + "<SPLIT>" + line[7]
    f.close()

    f = open('../pdf_modules/rules/model_rules.csv')
    rdr = csv.reader(f)
    returntext += "<FILE>"
    for line in rdr:
        returntext += "<LINE>" + line[0] + "<SPLIT>" + line[1]
    f.close()

    f = open('../pdf_modules/rules/xlsx_front_keys.csv')
    rdr = csv.reader(f)
    returntext += "<FILE>"
    for line in rdr:
        returntext += "<LINE>" + line[0]
    f.close()
    return returntext


@app.route('/ordersave', methods=['POST'])
def ordersave():
    """
    rule check 화면에서 rule 저장시에 호출
    :return:
    저장 성공 시에 "GOOD" String 반환
    """
    order_text = request.form["orderStr"]
    order_splited = order_text.split("<SPLIT>")
    f = open('../pdf_modules/rules/overall_model_rules_original.csv')
    rdr = csv.reader(f)
    overall_csv = list()
    for line in rdr:
        if (line[0] == order_splited[0].split("<SPLIT_VAL>")[0]):
            overall_csv.append(line)
    f.close()
    f = open('../pdf_modules/rules/rulemaking/after_overall_model_rules_tmp.csv')
    rdr = csv.reader(f)
    overall_after_csv = list()
    for line in rdr:
        if (line[0] != order_splited[0].split("<SPLIT_VAL>")[0]):
            overall_after_csv.append(line)
    f.close()
    for T in order_splited:
        if str(T.split("<SPLIT_VAL>")[0]) == str(order_splited[0].split("<SPLIT_VAL>")[0]):
            continue
        for M in overall_csv:
            if (M[1] == T.split("<SPLIT_VAL>")[0]):
                tmp_split = T.split("<SPLIT_VAL>")
                tmp_list = []
                for M_tmp in M:
                    tmp_list.append(M_tmp)
                tmp_list[5] = tmp_split[1]
                tmp_list[6] = tmp_split[2]
                tmp_list[7] = tmp_split[3]
                overall_after_csv.append(tmp_list)
                break

    f = open('../pdf_modules/rules/rulemaking/after_overall_model_rules_tmp.csv', 'w', encoding='utf-8', newline='')
    wr = csv.writer(f)
    for M in overall_after_csv:
        wr.writerow(M)
    f.close()

    db = make_db_collection()
    cursor = db.cursor()
    now = datetime.datetime.now()
    nowDatetime = str(now.strftime('%Y%2m%2d%2H%2M%2S'))
    sql = """
                                      INSERT INTO LOGS(LOGIN_ID,year,month,day,hour,minute,second,type)
                                      VALUES('seoil',%s,%s,%s,%s,%s,%s,3)
                                  """ % (
        nowDatetime[:4], nowDatetime[4:6], nowDatetime[6:8], nowDatetime[8:10], nowDatetime[10:12], nowDatetime[12:14])
    cursor.execute(sql)
    db.commit()
    db.close()

    return "GOOD"


"""
static 데이터 반환
"""


@app.route('/js/<js>', methods=['GET'])
def get_js(js):
    return send_file("static/js/" + js)


@app.route('/images/<images>', methods=['GET'])
def get_images(images):
    return send_file("static/images/" + images)


@app.route('/font/<font>', methods=['GET'])
def get_font(font):
    return send_file("static/font/" + font)


"""
일정 주기마다 tmp/uploaded 폴더 제거
"""


def print_date_time():
    shutil.rmtree("tmp/uploaded")
    directory_validation()


"""
tmp 폴더 및 tmp/uploaded 폴더 확인 & 생성
"""


def directory_validation():
    if not (os.path.isdir('tmp')):
        os.makedirs(os.path.join('tmp'))
    if not (os.path.isdir('tmp/uploaded')):
        os.makedirs(os.path.join('tmp/uploaded'))


"""
서버 시작
"""
if __name__ == "__main__":
    directory_validation()

    scheduler = BackgroundScheduler()
    scheduler.add_job(func=print_date_time, trigger="interval", minutes=30)
    scheduler.start()

    atexit.register(lambda: scheduler.shutdown())

    app.run(host="0.0.0.0", port=33353)
