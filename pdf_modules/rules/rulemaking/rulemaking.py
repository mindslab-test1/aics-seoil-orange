"""

rulemaking.py

해당 스크립트는 독립적으로 실행됩니다.
실행되었을 경우 현재 디렉토리 위의 original_model_rules_original.csv를 제작하기 위해 사용했던 프로세스입니다.


"""

import csv

overall_f = open("./overall_model_rules_tmp.csv")
rdr = csv.reader(overall_f)
overall_list = list()
for line in rdr:
    overall_list.append(line)
overall_f.close()


filename = [
    "ACP-2",
    "ACP-3",
    "ECH",
    "HPL-02",
    "SFA-1",
    "TDMAH",
    "TDMAS",
    "TEMAH",
    "TiCl4",
    "TMA",
    "USN-01"
]

for M in filename:
    f = open("./txt/" + M + ".txt",'r')
    while True:
        line = f.readline()
        if not line: break
        nowline = line[0:-1].split("\t")
        for i in range(len(overall_list)):
            if(overall_list[i][0]==M) and (overall_list[i][1]==nowline[0]):
                try:
                    if(len(nowline[1])>0):
                        overall_list[i][5] = nowline[1]
                except:
                    pass
                try:
                    if (len(nowline[2]) > 0):
                        overall_list[i][6] = nowline[2]
                except:
                    pass
                break;
            if(i==(len(overall_list))-1):
                print("Not included -> "+str(M) + "\t: " + str(nowline))

f = open("./after_overall_model_rules_tmp.csv", 'w', encoding='utf-8', newline='')
wr = csv.writer(f)
for T in overall_list:
    wr.writerow(T)
f.close()