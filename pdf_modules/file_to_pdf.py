import unicodedata

import pdftotext
from pdf_modules.string_to_key_val import string_to_key_val
from pdf_modules.key_val_postprocessing import *
from pdf_modules.pdf_str_util import remove_unit

"""
csv를 통한 rule의 경우 프로그램이 시작되며 import를 하는 시점에 load합니다.
해당 csv는 웹 상에서 변경하지 못하는 사항이기 때문에 변경된 사항을 반영을 위해선 서버 재시작이 필요합니다.
"""


f_mr = open('../pdf_modules/rules/model_rules.csv', 'r', encoding='utf-8')
model_rules = csv.reader(f_mr)
model_rules_list = list()
for T in model_rules:
    model_rules_list.append(T)
f_mr.close()
model_regex = "["
for T in model_rules_list:
    model_regex += T[1]
model_regex += "][0-9A-Z][0-3][0-9][1-9XYZ][0-9]"
model_cop = re.compile(model_regex)
cat_mr = open("../pdf_modules/rules/file_cat_rules.csv", 'r', encoding='utf-8')
cat_rules = csv.reader(cat_mr)
cat_rules_list = list()
for T in cat_rules:
    cat_rules_list.append(T)
cat_mr.close()


"""
cyl_no, lot_no, Product를 찾는 regex입니다.
"""
cyl_regex = "[Cc]yl[\s]*.[\s]*[Nn]o[\s.]*\[[^\[\]]*\]"
cyl_regex_re = re.compile(cyl_regex)
r_lot_regex = "L[oO][tT]\s*[\..]?\s*[Nn][Oo]\.?\s*\[\s*[0-9][0-9][0-9][0-9][0-9][0-9]"
r_lot_regex_re = re.compile(r_lot_regex)
product_regex = "roduct\s*:\s*[^\s]*"
product_regex_re = re.compile(product_regex)


def file_to_str(file_path):
    """
    PDF 파일 내부의 Table을 읽어 해당 Table의 값들을 Key와 Value 짝 형태의 tuple을 가진 list로 반환해줍니다.
    단, 해당 반환 값에서의 KEY값은 rule에 입각하여 변화된 형태로 반환됩니다.
    cyl, LotNo, Product는 모두 regex로 검색합니다.

    :param file_path: File name
    :return:
        overall_return_key_val_list
            TABLE에서의 KEY & Value 값을 가진 리스트
            Element : (<modified key>, <value>) tuple

        canister_no
            string

        r_lot_no
            string

        hpl_lot_no
            string

    """
    overall_return_key_val_list = list()
    canister_no = ""
    r_lot_no = ""
    hpl_lot_no = ""

    return_key_val = list()

    file = open(file_path, 'rb')
    filereader = pdftotext.PDF(file)

    # all_str : PDF -> Text 로 바꾼 Plain Text
    all_str = list()
    for M in filereader:
        M = unicodedata.normalize('NFKC', M)
        all_str.append(M)

    lot_by_filename = model_cop.findall(file_path.replace("БA","AND").replace("üA","AND").split("/")[-1])[-1]

    model = "Error"
    for K in model_rules_list:
        if (K[1] == lot_by_filename[0]):
            model = K[0]

    """
    File category의 경우 명시되지 않았을 경우 original, 그 외에 rule에 걸릴 경우 그에 맞게 카테고리를 변경합니다.
    """

    category = "Original"
    for M in cat_rules_list:
        if M[0] == cat_rules_list[0][0]:
            continue
        if M[0] in file_path.split("/")[-1]:
            category = M[1]
            break
    tmp_now_model = list()
    for pdf_page in all_str:
        try:
            now_model = product_regex_re.findall(pdf_page)[0].replace("roduct","").replace(":","").strip()
            if not (now_model in tmp_now_model) and (len(now_model)>1):
                tmp_now_model.append(now_model)
            else:
                continue
        except:
            pass

        # HPL의 경우에 lot_no 의 regex 규칙이 다릅니다.
        pdf_page = re.sub("10[-‐][0-9]\s?Pa","",pdf_page)
        pdf_page = pdf_page.replace("m3/","")
        cylno_list = cyl_regex_re.findall(pdf_page)
        if "ANALYTICAL" in pdf_page:
            r_lot_no_list = r_lot_regex_re.findall(pdf_page)
            if len(r_lot_no_list)>0:
                r_lot_no = r_lot_no_list[0].split("[")[-1].strip()
                if "Crude-HPL-02" in pdf_page:
                    hpl_lot_no = r_lot_no

        if(len(cylno_list)>0):
            canister_no = cylno_list[0].split("[")[-1].replace("]","").replace(" ","")
        tmp_key_val = string_to_key_val(pdf_page)
        # README #3
        # print(pdf_page)
        # print(tmp_key_val)
        for MM in tmp_key_val[2]:
            return_key_val.append((model, category, MM[0], remove_unit(MM[1]), MM[2]))

    del_list = list()
    for T in range(len(return_key_val)):
        for M in range(len(return_key_val)):
            if T >= M:
                continue
            if return_key_val[T][0]==return_key_val[M][0] and return_key_val[T][1]==return_key_val[M][1]\
                    and return_key_val[T][2]==return_key_val[M][2] and return_key_val[T][3]==return_key_val[M][3]:
                if not (T in del_list) and not (M in del_list):
                    del_list.append(M)

    for dl in range(len(del_list)):
        del return_key_val[del_list[dl]]
        for dll in range(len(del_list)):
            if dl<dll:
                del_list[dll] += -1

    for T in return_key_val:
        # 해당 부분에서 KEY를 모델이나 카테고리에 맞게 변경해줍니다.
        overall_return_key_val_list.append(key_val_postprocessing(T))

    return overall_return_key_val_list, canister_no, r_lot_no, hpl_lot_no


if __name__ == "__main__":
    """
    function test block
    """
    print(file_to_str("../test_files/TiCl4/0724/H12379.PDF"))