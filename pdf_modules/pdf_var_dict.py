change_cell_key_list = [
    ('Delivery', 'Delivery date'),
    ('delivery', 'Delivery date'),
    ('Filling', 'Filling date'),
    ('filling', 'Filling date'),
    ('Net weight', 'Net Weight'),
    ('net weight', 'Net Weight'),
    ('Net Weight', 'Net Weight'),
    ('net Weight', 'Net Weight'),
    ('Gross', 'Gross'),
    ('gross', 'gross'),
    ('Tare', 'Tare'),
    ('tare', 'Tare'),
    ('Net', 'Net'),
    ('net', 'Net'),
    ('Formula', 'Formula'),
    ('formula', 'Formula'),
    ('Maintenance', 'Maintenance date'),
    ('maintenance', 'Maintenance date'),
    ('', 'None'),
]


def change_cell_key(keyword):
    for T in change_cell_key_list:
        if T[0] in keyword:
            return T[1]
    return 'None'
