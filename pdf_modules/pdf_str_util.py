import re


def simplify_str(string_to_convert):
    ret_text = string_to_convert.upper()
    ret_text = re.sub("[^0-9a-zA-Z\-\_,.]", "", ret_text)
    return ret_text


def simplify_str_without_dash_comma(string_to_convert):
    ret_text = string_to_convert.upper()
    ret_text = re.sub("[^0-9a-zA-Z]", "", ret_text)
    return ret_text


unit_list = [
    "ppb(ng/g)",
    "pcs/ml",
    "°C",
    "kPa",
    "Mpa",
    "Sec",
    "% area",
    "MPa",
    "Pa・m3/sec"
]


def remove_unit(key_string):
    return_key_str = key_string
    # return_key_str = re.sub("[^0-9a-zA-Z]", "", return_key_str)
    for T in unit_list:
        return_key_str = return_key_str.replace(T, "").strip()
    return return_key_str


def rough_check_two_str(string1, string2):
    return simplify_str_without_dash_comma(string1) == simplify_str_without_dash_comma(string2)


def rough_check_list_str(string1, string2):
    first_list = string1.split("//")
    for M in first_list:
        if rough_check_two_str(M, string2):
            return True
    return False
