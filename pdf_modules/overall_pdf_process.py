from pdf_modules.pdf_relative_check import *
from pdf_modules.file_to_pdf import *

first_key_line = list()
f_key_line = open('../pdf_modules/rules/xlsx_front_keys.csv', 'r', encoding='utf-8')
first_key = csv.reader(f_key_line)
for T in first_key:
    first_key_line.append(T[0])
f_key_line.close()

f_mr = open('../pdf_modules/rules/model_rules.csv', 'r', encoding='utf-8')
model_rules = csv.reader(f_mr)
model_rules_list = list()
model_dict = dict()
for T in model_rules:
    model_rules_list.append(T)
    model_dict[T[1]] = T[0]
f_mr.close()


def overall_pick_check(dir_path):
    """
    전체적으로 파일의 변환작업을 수행
    :param dir_path: 한 번에 업로드 한 한 묶음의 PDF가 있는 디렉토리 path (string)
    :return: xlsx에 관련된 list
    """
    full_xlsx = list()

    f_overall_rules = open('../pdf_modules/rules/rulemaking/after_overall_model_rules_tmp.csv', 'r', encoding='utf-8')
    overall_rules = csv.reader(f_overall_rules)
    overall_rules_list = list()
    for T in overall_rules:
        overall_rules_list.append(T)
    f_overall_rules.close()

    # README #1
    now_xlsx_keys = first_key_line[:]
    overall_files = make_all_relative_files(dir_path)
    now_model_list = list()     # 현재 파일들의 모델 종류
    fixed_val_list = list()     # 고정밸류 목록
    overall_key_dict = dict()   # 현재 파일들의 총 output Key값 목록
    for file_elements in overall_files:
        for mrl in model_rules_list:
            if mrl[1] == file_elements[0][0]:
                if mrl[0] in now_model_list:
                    break
                else:
                    now_model_list.append(mrl[0])
    for ovl in overall_rules_list:
        if ovl[0] in now_model_list:
            if not (ovl[1] in now_xlsx_keys):
                now_xlsx_keys.append(ovl[1])
            if ovl[7] != "-":
                fixed_val_list.append((ovl[0], ovl[1], ovl[7]))
    for key_index in range(len(now_xlsx_keys)):
        overall_key_dict[str(now_xlsx_keys[key_index])] = key_index

    full_xlsx.append(now_xlsx_keys)
    for relative_files in overall_files:
        # README #2
        new_line = list()
        check_canister_no = ""
        check_r_lot_no = ""
        overall_hpl_lot_no = ""
        for i in range(len(now_xlsx_keys)):
            new_line.append("")
        new_line[0] = relative_files[0]
        for file_elem in relative_files:
            if file_elem == relative_files[0]:
                continue
            file_key_val, tmp_canister_no, tmp_r_lot_no, hpl_lot_no = file_to_str(file_elem)
            if tmp_canister_no != "":
                check_canister_no = tmp_canister_no
            if tmp_r_lot_no != "":
                check_r_lot_no = tmp_r_lot_no
            if hpl_lot_no != "":
                overall_hpl_lot_no = hpl_lot_no
            for fkv in file_key_val:
                try:
                    if fkv[0] != "":
                        new_line[overall_key_dict[fkv[1]]] = fkv[0]
                except:
                    print("ERROR in " + file_elem + ">>" + str(fkv))
        for mrl_count in model_rules_list:
            if new_line[0][0] == mrl_count[1]:
                for T in fixed_val_list:
                    if T[0] == mrl_count[0]:
                        if T[2] != "":
                            new_line[int(overall_key_dict[T[1]])] = T[2]
                            break

        new_line[int(overall_key_dict["LOT_NO"])] = relative_files[0]

        date_to_xlsx = relative_files[0][2:4]
        month_to_xlsx = {"1": "01", "2": "02", "3": "03", "4": "04", "5": "05", "6": "06", "7": "07", "8": "08",
                         "9": "09", "X": "10", "Y": "11", "Z": "12"}[relative_files[0][4]]
        if int(relative_files[0][5]) > 4:
            year_to_xlsx = "1" + relative_files[0][5]
        else:
            year_to_xlsx = "2" + relative_files[0][5]

        new_line[int(overall_key_dict["MANU_DATE"])] = "20" + year_to_xlsx + month_to_xlsx + date_to_xlsx
        new_line[int(overall_key_dict["TEST_DATE"])] = "20" + year_to_xlsx + month_to_xlsx + date_to_xlsx
        try:
            new_line[int(overall_key_dict["CANISTER_NO"])] = check_canister_no.split("/")[0]
        except:
            pass
        try:
            new_line[int(overall_key_dict["R_LOT_NO"])] += check_r_lot_no
        except:
            pass

        try:
            if new_line[0][0] == "0":
                new_line[int(overall_key_dict["R_LOT_NO"])] = overall_hpl_lot_no

        except:
            pass

        try:
            if new_line[0][0] == "E":
                new_line[int(overall_key_dict["F_PURITY"])] = new_line[int(overall_key_dict["F_PURITY_METAL"])]
        except:
            pass

        try:
            now_models = model_dict[new_line[0][0]]
            for ovl_fixed_val in overall_rules_list:
                if ovl_fixed_val[0] == now_models:
                    if ovl_fixed_val[7] != "-":
                        new_line[int(overall_key_dict[ovl_fixed_val[1]])] = ovl_fixed_val[7]

        except:
            pass

        full_xlsx.append(new_line)
    return full_xlsx
