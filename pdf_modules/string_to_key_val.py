import csv
from pdf_modules.pdf_str_util import *

information_regex = re.compile("[^\-\=\[\]\n]*\[[^\[\]\n]*\]")
key_val_list_regex = re.compile("(\-*[0-9]+\.[0-9]+)|(\-*[0-9]+)")
japan_grossregex = re.compile("[Gg][Rr][Oo][Ss][Ss]\s*\([0-9\.\s]*")
japan_tareregex = re.compile("[Tt][Aa][Rr][Ee]\s*\([0-9\.\s]*")
japan_netregex = re.compile("[Nn][Ee][Tt]\s*\([0-9\.\s]*")

f_infor_to_keyval = open("../pdf_modules/rules/information_to_keyval.csv", 'r', encoding='utf-8')
infor_to_keyval = csv.reader(f_infor_to_keyval)
infor_to_keyval_list = list()
for T in infor_to_keyval:
    infor_to_keyval_list.append(T)
f_infor_to_keyval.close()

product_regex = "[Pp]roduct\s*:[^\n]*"
product_cop = re.compile(product_regex)

comment_list = [
    "Particle counter",
    "Moisture analyzer",
    "ICP-MS"
]


def string_to_key_val(string_to_convert):
    printdebug = False
    if (printdebug): print(string_to_convert)

    product = product_cop.findall(string_to_convert)
    try:
        product_name = product[0]
        product_name = product_name.split(":")[1]
        if (product_name.find("Phone") != -1):
            product_name = product_name.split("Phone")[0]
        product_name = product_name.strip()
    except:
        product_name = ""
    overall_str = string_to_convert.split("\n")
    information_list = list()
    key_val_list = list()
    try:
        information_list.append(("PLANT", overall_str[1].strip()))
    except:
        pass

    for M in overall_str:
        mc = information_regex.findall(M)
        for mcM in mc:
            information_list.append((mcM.split("[")[0].replace(" ", ""),
                                     mcM.split("[")[1].replace("]", "").replace(" ", "")))
        j_gross = japan_grossregex.findall(M)
        for mcM in j_gross:
            information_list.append(("Gross", mcM.split("(")[-1].replace(" ", "")))
        j_tare = japan_tareregex.findall(M)
        for mcM in j_tare:
            information_list.append(("Tare", mcM.split("(")[-1].replace(" ", "")))
        j_net = japan_netregex.findall(M)
        for mcM in j_net:
            information_list.append(("Net", mcM.split("(")[-1].replace(" ", "")))

    now_table = False
    next_skip = 0
    for column in range(len(overall_str)):
        try:
            if (column == len(overall_str) - 1):
                break
            if (next_skip > 0):
                next_skip += -1
                continue
            if ("Analysis" in overall_str[column]) and ("Parameter" in overall_str[column]):
                now_table = True
                continue
            if ("Judgment" in overall_str[column]):
                break
            if (now_table):
                tmp_find_comment = False
                for TCL in comment_list:
                    if (TCL in overall_str[column]):
                        tmp_find_comment = True
                # now Key Value checking Processing
                if ("Can" in overall_str[column]) and (overall_str[column + 1][0:5] == "     ") and (
                        overall_str[column + 2][0:5] == "     "):
                    key = remove_unit(overall_str[column].replace("\t", "   ").split("  ")[0].replace(" ", ""))
                    val = key_val_list_regex.findall((overall_str[column] + overall_str[column + 2]).strip())[-1][0] \
                          + key_val_list_regex.findall((overall_str[column] + overall_str[column + 2]).strip())[-1][1]
                    next_skip = 2
                    key_val_list.append((product_name, key, val))
                elif (overall_str[column + 1][0:5] == "     ") and not ("Judgment" in overall_str[column + 1]) and not (
                tmp_find_comment):
                    if (printdebug): print("TTT" + str(overall_str[column]))
                    key = remove_unit(overall_str[column].replace("\t", "   ").split("  ")[0].replace(" ", ""))
                    if (key.find("]") == -1) and (key.find("[") == -1) and len(key) > 1:
                        if (printdebug): print(
                            key_val_list_regex.findall(overall_str[column] + overall_str[column + 1]))
                        val = key_val_list_regex.findall((overall_str[column] + overall_str[column + 1]).strip())[-1][0] \
                              + key_val_list_regex.findall((overall_str[column] + overall_str[column + 1]).strip())[-1][
                                  1]
                        if (printdebug): print(key, val)
                        if (val.strip() == ""):
                            val = key_val_list_regex.findall(overall_str[column])[-1][0]
                            next_skip = 0
                        if (printdebug): print("APPENDED => " + str((product_name, key, val)))
                        key_val_list.append((product_name, key, val))
                    next_skip = 1
                elif (len(key_val_list_regex.findall(overall_str[column])) == 0):
                    if (printdebug): print("string_to_key_val CONTINUE")
                    continue
                else:
                    if (printdebug): print(overall_str[column].split("  "))
                    key = remove_unit(overall_str[column].split("  ")[0].replace(" ", ""))
                    if (key.find("]") == -1) and (key.find("[") == -1) and len(key) > 0:
                        val = key_val_list_regex.findall(overall_str[column])[-1][0] + \
                              key_val_list_regex.findall(overall_str[column])[-1][1]
                        if (printdebug): print("APPENDED => " + str((product_name, key, val)))
                        key_val_list.append((product_name, key, val))
        except:
            print("EXCEPT in " + str(overall_str[column]))
            pass

    for T in infor_to_keyval_list:
        for M in information_list:
            if rough_check_two_str(T[0], M[0]):
                key_val_list.append((product_name, T[1], M[1]))

    return product_name, information_list, key_val_list
