import csv
from pdf_modules.pdf_str_util import *


def key_val_postprocessing(key_val_list_element):
    """
    KEY와 Value를 받아 두 가지의 기능을 합니다.
        1. Key를 모델과 카테고리에 맞게 변경하는 작업
            같은 Key를 가지고 있어도 모델에 따라 Key의 출력값이 바뀝니다.
            현재 Key가 어느 모델에서 어떻게 바뀐 이름을 가지는지에 대해 rule로 판단하여 replace합니다.
        2. Value의 최대 및 최소 확인
            현재 value가 rule로 지정되어있는 값보다 작은지 큰지 확인하는 작업이 진행됩니다.
            해당 최대 최소 범위를 넘어갈 경우 이는 ERROR를 포함한 string으로 반환되어 클라이언트의 웹 브라우져 상에서 표기됩니다.
    :param key_val_list_element: (Model, Category, KEY, Value)
    :return:
        1. 범위내의 value 값을 가지고 있을 경우
            변경된 키값, value값
        2. 범위를 벗어난 value값을 가지고 있을 경우
            input parameter, '_'
    """
    f_overall_rules = open('../pdf_modules/rules/rulemaking/after_overall_model_rules_tmp.csv', 'r', encoding='utf-8')
    overall_rules = csv.reader(f_overall_rules)
    overall_rules_list = list()
    for T in overall_rules:
        overall_rules_list.append(T)
    f_overall_rules.close()
    for M in overall_rules_list:
        if M[0] == overall_rules_list[0][0]:
            continue
        if M[0] == key_val_list_element[0]:
            if M[3] == key_val_list_element[1]:
                if rough_check_two_str(M[4], key_val_list_element[2]) or M[4] == '0':
                    if rough_check_list_str(M[2], key_val_list_element[3]):

                        if (M[7] != "-"):
                            return M[7], M[1]
                        tmp_min = -999999.999999
                        tmp_max = 999999.999999
                        if (M[5] != "-"):
                            tmp_min = float(M[5])
                        if (M[6] != "-"):
                            tmp_max = float(M[6])
                        try:
                            if (float(key_val_list_element[4]) > tmp_max) or (float(key_val_list_element[4]) < tmp_min):
                                return "SPEC ERROR**" + str(key_val_list_element[4]), M[1]
                        except:
                            return key_val_list_element[4], M[1]
                        return key_val_list_element[4], M[1]
    for M in overall_rules_list:
        if M[0] == overall_rules_list[0][0]:
            continue
        if M[0] == key_val_list_element[0]:
            if M[3] == key_val_list_element[1] or M[3] == '0':
                if rough_check_two_str(M[4], key_val_list_element[2]):
                    if rough_check_list_str(M[2], key_val_list_element[3]):


                        if (M[7] != "-"):
                            return M[7], M[1]
                        tmp_min = -999999.999999
                        tmp_max = 999999.999999
                        if (M[5] != "-"):
                            tmp_min = float(M[5])
                        if (M[6] != "-"):
                            tmp_max = float(M[6])
                        try:
                            if (float(key_val_list_element[4]) > tmp_max) or (float(key_val_list_element[4]) < tmp_min):
                                return "SPEC ERROR**" + str(key_val_list_element[4]), M[1]
                        except:
                            return key_val_list_element[4], M[1]
                        return key_val_list_element[4], M[1]

    for M in overall_rules_list:
        if M[0] == overall_rules_list[0][0]:
            continue
        if M[0] == key_val_list_element[0]:
            if M[3] == key_val_list_element[1] or M[3] == '0':
                if rough_check_two_str(M[4], key_val_list_element[2]) or M[4] == '0':
                    if rough_check_list_str(M[2], key_val_list_element[3]):

                        if (M[7] != "-"):
                            return M[7], M[1]
                        tmp_min = -999999.999999
                        tmp_max = 999999.999999
                        if (M[5] != "-"):
                            tmp_min = float(M[5])
                        if (M[6] != "-"):
                            tmp_max = float(M[6])
                        try:
                            if (float(key_val_list_element[4]) > tmp_max) or (float(key_val_list_element[4]) < tmp_min):
                                return "SPEC ERROR**" + str(key_val_list_element[4]), M[1]
                        except:
                            return key_val_list_element[4], M[1]
                        return key_val_list_element[4], M[1]

    return "ERROR (" + str(key_val_list_element) + ")", "_"
