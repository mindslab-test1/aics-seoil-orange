import re


def simplify_str(string_to_convert):
    ret_text = string_to_convert.upper()
    ret_text = re.sub("[^0-9a-zA-Z\-\_,.]","",ret_text)
    return ret_text


def simplify_str_without_dash_comma(string_to_convert):
    ret_text = string_to_convert.upper()
    ret_text = re.sub("[^0-9a-zA-Z]","",ret_text)
    return ret_text


unit_list = [
    "ppb(ng/g)",
    "pcs/ml",
    "°C",
    "kPa",
    "Mpa",
    "Sec",
    "% area"
]


def remove_unit(key_string):
    for T in unit_list:
        key_string = key_string.replace(T, "").strip()
    return key_string


def rough_check_two_str(string1, string2):
    return simplify_str_without_dash_comma(string1) == simplify_str_without_dash_comma(string2)