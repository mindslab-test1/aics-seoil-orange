"""
업로드 된 여러 파일에서 서로 연관성을 찾아주는 함수 정의 스크립트

"""

import csv
import os
import re

f_mr = open('../pdf_modules/rules/model_rules.csv', 'r', encoding='utf-8')
model_rules = csv.reader(f_mr)
model_rules_list = list()
for T in model_rules:
    model_rules_list.append(T)
f_mr.close()


def search(all_files, dirname):
    try:
        filenames = os.listdir(dirname)
        for filename in filenames:
            full_filename = os.path.join(dirname, filename)
            if os.path.isdir(full_filename):
                search(all_files, full_filename)
            else:
                ext = os.path.splitext(full_filename)[-1]
                if ext == '.pdf' or ext == '.PDF':
                    all_files.append(full_filename)
    except PermissionError:
        pass


def search_recur(dirname):
    all_files = list()
    search(all_files, dirname)
    return all_files


def search_relative_files(all_files):
    original_files = list()
    for K in all_files:
        original_file_name = K.split("/")[-1].replace(".pdf", "").replace(".PDF", "")
        if len(original_file_name) == 6:
            original_files.append((K, original_file_name))
    return original_files


def check_relative_files(all_files, original_file_name_param):
    relative_files = list()
    for T in all_files:
        if original_file_name_param in T:
            relative_files.append(T)
    return relative_files


def find_scope_files(dir_path):
    generated_model_regex = "["
    for T in model_rules_list:
        generated_model_regex += T[1]
    generated_model_regex += "][0-9A-Z][0-3][0-9][1-9XYZ][0-9]"
    orilist = list()
    files = search_recur(dir_path)
    scope_file_names = list()
    model_regex = re.compile(generated_model_regex)
    scope_regex = re.compile(generated_model_regex + '[\s_]*[\-\_][\s_]*' + generated_model_regex)
    dict_hexa = {"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9,
                 "A": 10, "B": 11, "C": 12, "D": 13, "E": 14, "F": 15, "G": 16, "H": 17, "I": 18, "J": 19, "K": 20,
                 "L": 21, "M": 22, "N": 23, "O": 24, "P": 25, "Q": 26, "R": 27, "S": 28, "T": 29, "U": 30, "V": 31,
                 "W": 32, "X": 33, "Y": 34, "Z": 35}
    list_hexa = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                 "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    for file in files:
        tmp_filename = file[:]
        file =file.replace("БA","AND").replace("üA","AND")
        tmp_now_models = list()
        for SRF in scope_regex.findall(file.split("/")[-1]):
            for SRFi in range(dict_hexa[SRF.replace("_","-").replace("_","").replace(" ","").split("-")[0].strip()[1]], dict_hexa[SRF.replace("БA","AND").replace("_","-").replace("_","").replace(" ","").split("-")[1].strip()[1]] + 1):
                tmpappend = SRF[0] + list_hexa[SRFi] + SRF[2:6]
                if not (tmpappend in tmp_now_models):
                    tmp_now_models.append(tmpappend)
        for MRF in model_regex.findall(file.split("/")[-1]):
            if not (MRF in tmp_now_models):
                tmp_now_models.append(MRF)
        for orimodels in tmp_now_models:
            if not (orimodels in orilist):
                orilist.append(orimodels)
        scope_file_names.append((tmp_filename, tmp_now_models))
    return orilist, scope_file_names


def overall_model_connect(dir_path):
    overall_connect_list = list()
    oridict = dict()
    ori, find_scope = find_scope_files(dir_path)
    for T in range(len(ori)):
        oridict[ori[T]] = T
        overall_connect_list.append([ori[T]])
    for FSF in find_scope:
        for FSF_temp in FSF[1]:
            overall_connect_list[oridict[FSF_temp]].append(FSF[0])
    return overall_connect_list


def make_all_relative_files(dirpath):
    overall_list = overall_model_connect(dirpath)
    return overall_list

# for OMC in make_all_relative_files("../pdf/korea/ACP-3_20년도 Data/0327"):
#     print(OMC)
