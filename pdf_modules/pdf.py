import os
import re
import pdftotext as pdftotext
import unicodedata

from pdf_modules.pdf_util import remove_unit
from pdf_modules.pdf_var_dict import change_cell_key


def pdf_read_regex(file_path):
    file = open(file_path, 'rb')
    fileReader = pdftotext.PDF(file)
    all_str = ""
    all_str += str(fileReader[0])
    all_str = unicodedata.normalize('NFKC', all_str)

    variables_dict = dict()

    variables_regex = re.compile("[^\[\]\n]*\[[^\[\]]*\]")
    variables_regex_find = variables_regex.findall(all_str)

    variables_sec_regex = re.compile("\[[^\[\]]*\]")
    for T in variables_regex_find:
        if not (change_cell_key(T) == 'None'):
            variables_dict[change_cell_key(T)] = variables_sec_regex.findall(T)[-1][1:-2].strip()

    LotNo_regex = re.compile("[A-Z][A-Za-z0-9]{5}")
    CylNo_regex = re.compile("[A-Z][0-9]{3}/")
    product_regex = re.compile("[Pp]roduct\s*:\s*[A-Z\s]*")

    CylNo = "     "
    LotNo = "Error"
    product = "     "
    LotNo_regex_find = LotNo_regex.findall(file_path)
    product_regex_find = product_regex.findall(all_str)
    for prf in product_regex_find:
        product = prf[1:].split("        ")[0].replace("roduct", "").replace(":", "").strip()
    for LR in LotNo_regex_find:
        LotNo = LR
    for T in variables_regex_find:
        print("variables_regex_find >> " + T)
        now_value = T[1:-1].strip()

        CylNo_regex_find = CylNo_regex.findall(now_value)

        for CR in CylNo_regex_find:
            CylNo = CR[:-1]


    str_list = all_str.split("\n")
    ret_str_list = list()
    toggle_switch = False
    for T in str_list:
        if ("Analysis Parameter" in T):
            toggle_switch = True
            continue
        elif ("Judgment" in T):
            toggle_switch = False
            break
        if (toggle_switch == True):
            # print(T)
            ret_str_list.append(T)

    key_value_list = list()
    for T in ret_str_list:
        now_space_count = 0
        now_space_name = ""
        for M in str(T):
            if " " in M:
                now_space_count += 1
            else:
                now_space_count = 0
            if (now_space_count > 2):
                break
            now_space_name += M
        variables_values = re.compile("\s[0-9.]+")
        variables_values_find = variables_values.findall(str(T))
        try:
            key_value_list.append((remove_unit(now_space_name.strip()), (variables_values_find[-1].strip())))
        except:
            pass

    cursor = 0
    try:
        while (True):
            if (cursor >= len(key_value_list)):
                break
            if (len(key_value_list[cursor][0]) < 1):
                key_value_list[cursor - 1] = (key_value_list[cursor - 1][0], key_value_list[cursor][1])
                del key_value_list[cursor]
            else:
                cursor += 1
    except:
        pass

    return [CylNo, LotNo, product, file_path, key_value_list, variables_dict]


all_files = list()


def search(dirname):
    try:
        filenames = os.listdir(dirname)
        for filename in filenames:
            full_filename = os.path.join(dirname, filename)
            if os.path.isdir(full_filename):
                search(full_filename)
            else:
                ext = os.path.splitext(full_filename)[-1]
                if ext == '.pdf' or ext == '.PDF':
                    all_files.append(full_filename)
    except PermissionError:
        pass


def directory_pdf_checking(dir_path):
    search(dir_path)

    category_list = list()
    pdf_list = list()
    for af in all_files:
        pdf_list.append(pdf_read_regex(af))

    for T in pdf_list:
        for category_tuples in T[2]:
            if not (category_tuples[0] in category_list):
                print("Put in -> " + category_tuples[0])
                category_list.append(category_tuples[0])


    for M in category_list:
        print(M)


def print_pdf_output_read_regex(pdf_output):
    print("File : " + pdf_output[1])
    print("Cyl No : " + pdf_output[0])
    print("Model : " + pdf_output[2])

    print("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<     Value     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    for key in pdf_output[5].keys():
        print('%50s\t%50s' % (key, pdf_output[5][key]))
    print("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<     Table     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    for K in pdf_output[4]:
        print('%50s\t%50s' % (K[0], K[1]))


def file_to_response(filename):
    tmp = pdf_read_regex(filename)
    retstr = ""
    retstr += ("0<STATUS>" + tmp[1].split("/")[-1][36:])
    retstr += ("<SPLIT>1<STATUS>" + tmp[0])
    for key in tmp[5].keys():
        retstr += "<SPLIT>3<STATUS>" + key + "<VAL>" + tmp[5][key]
    retstr += "<SPLIT>4<STATUS>" + "<TABLE>"
    for K in tmp[4]:
        retstr += "<SPLIT>5<STATUS>" + K[0] + "<VAL>" + K[1]
    return retstr


def file_to_overall_keys(filename):
    print(filename)


# directory_pdf_checking("./pdf")
# all_files = search("./pdf")
# print("CylNo\t\tLotNo\t\tFile_path")
# for M in all_files:
#     pdf_read_regex(M)


# tmp = pdf_read_regex("./pdf/korea/TMA_20년도 Data/0121/X11010.pdf")
# print_pdf_output_read_regex(tmp)

# print(file_to_response("./pdf/korea/TMA_20년도 Data/0121/X11010.pdf"))
